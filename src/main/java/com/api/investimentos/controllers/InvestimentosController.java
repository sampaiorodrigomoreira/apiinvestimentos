package com.api.investimentos.controllers;

import com.api.investimentos.models.Investimento;
import com.api.investimentos.services.InvestimentosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/investimentos")
public class InvestimentosController {

    @Autowired
    private InvestimentosService investimentosService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Investimento cadastrarInvestimento(@RequestBody @Valid Investimento investimento){

        Investimento objInvestimento = investimentosService.cadastrarInvestimento(investimento);
        return objInvestimento;

    }

    @GetMapping
    public Iterable<Investimento> lerTodosOsInvestimentos(){
        return investimentosService.lerTodosOsInvestimentos();
    }

}
