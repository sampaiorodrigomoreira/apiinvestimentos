package com.api.investimentos.controllers;

import com.api.investimentos.models.Investimento;
import com.api.investimentos.models.Simulacao;
import com.api.investimentos.services.InvestimentosService;
import com.api.investimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/simulacoes")
public class SimulacaoController {

    @Autowired
    private SimulacaoService simulacaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Simulacao cadastrarSimulacao(@RequestBody @Valid Simulacao simulacao){

        Simulacao objSimulacao = simulacaoService.cadastrarSimulacao(simulacao);
        return objSimulacao;

    }


}
