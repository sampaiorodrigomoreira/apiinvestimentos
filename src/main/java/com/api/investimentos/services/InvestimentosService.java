package com.api.investimentos.services;

import com.api.investimentos.models.Investimento;
import com.api.investimentos.repositories.InvestimentosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InvestimentosService {

    @Autowired
    private InvestimentosRepository investimentosRepository;

    public Investimento cadastrarInvestimento(Investimento investimento){
        Investimento objinv = investimentosRepository.save(investimento);
        return  objinv;
    }

    //retorna todos os investimentos
    public Iterable<Investimento> lerTodosOsInvestimentos(){
        return investimentosRepository.findAll();
    }



}
