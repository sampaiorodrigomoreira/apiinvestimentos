package com.api.investimentos.services;

import com.api.investimentos.models.Investimento;
import com.api.investimentos.models.Simulacao;
import com.api.investimentos.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.ResponseStatusException;

@Service
public class SimulacaoService {

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    @PostMapping("/{id}")
    public Simulacao cadastrarSimulacao(@PathVariable(name = "id") Simulacao simulacao){
        try{

            Simulacao objSimulacao = simulacaoRepository.save(simulacao);
            return  objSimulacao;

        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }



}
