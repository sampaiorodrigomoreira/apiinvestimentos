package com.api.investimentos.repositories;

import com.api.investimentos.models.Investimento;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentosRepository extends CrudRepository<Investimento, Integer> {
}
