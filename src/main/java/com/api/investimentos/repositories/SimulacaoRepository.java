package com.api.investimentos.repositories;

import com.api.investimentos.models.Simulacao;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository  extends CrudRepository<Simulacao, Integer> {

}
