package com.api.investimentos.models;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "investimentos")
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @NotBlank
    @Size(min = 5)
    private String nome;

    @NotNull
    @Digits(integer = 6, fraction = 2, message = "investimento fora do padrão")
    @DecimalMin(value = "1.0", message = "O investimento minimo é de 1.0")
    private double rendimentoAoMes;

    public Investimento() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }
}
